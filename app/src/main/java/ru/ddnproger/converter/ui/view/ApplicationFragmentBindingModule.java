package ru.ddnproger.converter.ui.view;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.ddnproger.converter.ui.view.currency.list.CurrencyListFragment;
import ru.ddnproger.converter.ui.view.currency.main.CurrencyMainFragment;

@Module
public abstract class ApplicationFragmentBindingModule {

    @ContributesAndroidInjector
    abstract CurrencyMainFragment provideMainFragment();

    @ContributesAndroidInjector
    abstract CurrencyListFragment provideListFragment();
}
