package ru.ddnproger.converter.ui.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;

import ru.ddnproger.converter.R;
import ru.ddnproger.converter.ui.common.BaseActivity;
import ru.ddnproger.converter.ui.view.currency.main.CurrencyMainFragment;

public class ApplicationActivity extends BaseActivity {

    @Override
    protected int layoutRes() {
        return R.layout.activity_application;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, new CurrencyMainFragment()).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
