package ru.ddnproger.converter.ui.viewModel.currency.main;

import android.arch.lifecycle.MutableLiveData;
import android.text.TextUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.ddnproger.converter.ui.common.BaseViewModel;
import ru.ddnproger.domain.entity.CurrencyEntity;
import ru.ddnproger.domain.interactor.CurrencyInteractor;
import ru.ddnproger.domain.interactor.SynchronizeInteractor;

public class CurrencyMainViewModel extends BaseViewModel {

    private final CurrencyInteractor interactor;
    private final SynchronizeInteractor synchronizeInteractor;

    private final MutableLiveData<CurrencyEntity> sourceCurrency = new MutableLiveData<>();
    private final MutableLiveData<CurrencyEntity> destCurrency = new MutableLiveData<>();
    private final MutableLiveData<String> destValue = new MutableLiveData<>();
    private final MutableLiveData<String> lastUpdate = new MutableLiveData<>();

    @Inject
    public CurrencyMainViewModel(CurrencyInteractor interactor, SynchronizeInteractor synchronizeInteractor) {
        this.interactor = interactor;
        this.synchronizeInteractor = synchronizeInteractor;
        synchronize();
    }

    public void onResume() {
        fetch();
    }

    public MutableLiveData<CurrencyEntity> getSourceCurrency() {
        return sourceCurrency;
    }

    public MutableLiveData<CurrencyEntity> getDestCurrency() {
        return destCurrency;
    }

    public MutableLiveData<String> getDestValue() {
        return destValue;
    }

    public MutableLiveData<String> getLastUpdate() {
        return lastUpdate;
    }

    private void synchronize() {
        Disposable disposable = synchronizeInteractor.synchronize()
                .subscribe(() -> lastUpdate.setValue(interactor.getLastUpdate()), Throwable::printStackTrace);

        unsubscribeOnDestroy(disposable);
    }

    private void fetch() {
        sourceCurrency.setValue(interactor.getSourceCurrency());
        destCurrency.setValue(interactor.getDestCurrency());
        lastUpdate.setValue(interactor.getLastUpdate());
    }

    public void swap() {
        interactor.swapCurrency();
        fetch();
    }

    public void calculate(String source) {
        if (destCurrency.getValue() == null || sourceCurrency.getValue() == null) {
            return;
        }

        if (TextUtils.isEmpty(source)) {
            destValue.setValue("0");
            return;
        }

        BigDecimal result = sourceCurrency.getValue().calculate(
                new BigDecimal(source),
                destCurrency.getValue());
        result = result.setScale(2, RoundingMode.CEILING);

        destValue.setValue(String.format("%s %s", result.toString(), destCurrency.getValue().getCurrencySymbol()));
    }
}
