package ru.ddnproger.converter.ui.view.currency.list;

import android.arch.lifecycle.LifecycleOwner;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.ddnproger.converter.R;
import ru.ddnproger.converter.ui.viewModel.currency.list.CurrencyListViewModel;
import ru.ddnproger.domain.entity.CurrencyEntity;

public class CurrencyListAdapter extends RecyclerView.Adapter<CurrencyListAdapter.CurrencyViewHolder> {

    private CurrencySelectedListener currencySelectedListener;
    private final List<CurrencyEntity> data = new ArrayList<>();
    private final CurrencyEntity currentCurrency;

    public CurrencyListAdapter(CurrencyListViewModel viewModel, CurrencyEntity currentCurrency, LifecycleOwner lifecycleOwner, CurrencySelectedListener currencySelectedListener) {
        this.currencySelectedListener = currencySelectedListener;
        this.currentCurrency = currentCurrency;
        viewModel.getCurrencies().observe(lifecycleOwner, currencyList -> {
            data.clear();
            if (currencyList != null) {
                data.addAll(currencyList);
                notifyDataSetChanged();
            }
        });
    }

    @NonNull
    @Override
    public CurrencyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new CurrencyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_currency_list_item, viewGroup, false), currencySelectedListener, currentCurrency);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyViewHolder currencyViewHolder, int i) {
        currencyViewHolder.bind(data.get(i));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static final class CurrencyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textName)
        AppCompatTextView textName;

        private final CurrencyEntity currentCurrency;

        private CurrencyEntity currency;

        public CurrencyViewHolder(@NonNull View itemView, CurrencySelectedListener selectedListener, CurrencyEntity currentCurrency) {
            super(itemView);
            this.currentCurrency = currentCurrency;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if (currency != null) selectedListener.onCurrencySelected(currency);
            });
        }

        public void bind(CurrencyEntity currency) {
            this.currency = currency;
            textName.setText(currency.getName());
            if (currency.equals(currentCurrency)) {
                textName.setTextColor(itemView.getContext().getResources().getColor(R.color.colorAccent));
            } else{
                textName.setTextColor(itemView.getContext().getResources().getColor(R.color.colorTextPrimary));
            }
        }
    }
}
