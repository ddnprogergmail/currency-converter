package ru.ddnproger.converter.ui.view.currency.list;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.ddnproger.converter.R;
import ru.ddnproger.converter.ui.common.BaseFragment;
import ru.ddnproger.converter.ui.common.ViewModelFactory;
import ru.ddnproger.converter.ui.viewModel.currency.list.CurrencyListViewModel;
import ru.ddnproger.domain.entity.CurrencyEntity;

public class CurrencyListFragment extends BaseFragment implements CurrencySelectedListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.viewError)
    View viewError;
    @BindView(R.id.viewLoading)
    View viewLoading;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    ViewModelFactory viewModelFactory;

    private CurrencyListViewModel viewModel;
    private SelectionType type;

    @Override
    protected int layoutRes() {
        return R.layout.fragment_currency_list;
    }

    public static CurrencyListFragment getInstance(SelectionType type) {
        CurrencyListFragment instance = new CurrencyListFragment();
        Bundle args = new Bundle();
        args.putSerializable("type", type);
        instance.setArguments(args);

        return instance;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getArguments() == null || !getArguments().containsKey("type")) {
            throw new IllegalStateException("Selection type not set");
        }

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CurrencyListViewModel.class);

        type = (SelectionType) getArguments().getSerializable("type");
        getBaseActivity().setSupportActionBar(toolbar);
        getBaseActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView.setAdapter(new CurrencyListAdapter(viewModel, type == SelectionType.SOURCE ? viewModel.getCurrentSource() : viewModel.getCurrentDest(), this, this));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                ((LinearLayoutManager) recyclerView.getLayoutManager()).getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        observableViewModel();
    }

    @Override
    public void onCurrencySelected(CurrencyEntity currency) {
        viewModel.setCurrency(currency, type);
        getBaseActivity().onBackPressed();
    }

    @OnClick(R.id.buttonRetry)
    void onButtonRetryClick() {
        viewModel.retry();
    }

    private void observableViewModel() {
        viewModel.getCurrencies().observe(this, currencyList -> {
            if (currencyList != null) recyclerView.setVisibility(View.VISIBLE);
        });

        viewModel.getError().observe(this, isError -> {
            if (isError != null && isError) {
                viewError.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            } else viewError.setVisibility(View.GONE);
        });

        viewModel.getLoading().observe(this, isLoading -> {
            if (isLoading != null) {
                viewLoading.setVisibility(isLoading ? View.VISIBLE : View.GONE);
                if (isLoading) {
                    viewError.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                }
            }
        });
    }
}
