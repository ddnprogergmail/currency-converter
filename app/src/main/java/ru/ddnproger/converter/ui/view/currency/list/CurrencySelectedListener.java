package ru.ddnproger.converter.ui.view.currency.list;

import ru.ddnproger.domain.entity.CurrencyEntity;

public interface CurrencySelectedListener {

    void onCurrencySelected(CurrencyEntity currency);
}
