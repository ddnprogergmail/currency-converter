package ru.ddnproger.converter.ui.view.currency.main;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.ddnproger.converter.R;
import ru.ddnproger.converter.ui.common.BaseFragment;
import ru.ddnproger.converter.ui.common.DecimalDigitsInputFilter;
import ru.ddnproger.converter.ui.common.ViewModelFactory;
import ru.ddnproger.converter.ui.view.currency.list.CurrencyListFragment;
import ru.ddnproger.converter.ui.view.currency.list.SelectionType;
import ru.ddnproger.converter.ui.viewModel.currency.main.CurrencyMainViewModel;

public class CurrencyMainFragment extends BaseFragment {

    @BindView(R.id.textLastUpdate)
    AppCompatTextView textLastUpdate;
    @BindView(R.id.editSourceValue)
    AppCompatEditText editSource;
    @BindView(R.id.textResult)
    AppCompatTextView textResult;

    @BindView(R.id.viewInputs)
    View viewInputs;

    @BindView(R.id.textSourceCurrency)
    AppCompatTextView textSourceCurrency;
    @BindView(R.id.textDestCurrency)
    AppCompatTextView textDestCurrency;

    @Inject
    ViewModelFactory viewModelFactory;

    private CurrencyMainViewModel viewModel;

    @Override
    protected int layoutRes() {
        return R.layout.fragment_currency_main;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CurrencyMainViewModel.class);
        editSource.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(6, 2)});
        editSource.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1 && s.toString().equalsIgnoreCase(".")) {
                    editSource.setText("0.");
                    editSource.setSelection(2);
                    return;
                }
                viewModel.calculate(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        observableViewModel();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.onResume();
    }

    @OnClick(R.id.buttonSelectSourceCurrency)
    void onButtonSelectSourceCurrencyClick() {
        getBaseActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, CurrencyListFragment.getInstance(SelectionType.SOURCE)).addToBackStack(null).commit();
    }

    @OnClick(R.id.buttonSelectDestCurrency)
    void onButtonSelectDestCurrencyClick() {
        getBaseActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, CurrencyListFragment.getInstance(SelectionType.DEST)).addToBackStack(null).commit();
    }

    @OnClick(R.id.buttonSwap)
    void onButtonSwapClick() {
        viewModel.swap();
        viewModel.calculate(editSource.getText().toString());
    }

    private void observableViewModel() {
        viewModel.getSourceCurrency().observe(this, currencyEntity -> {
            viewInputs.setVisibility((currencyEntity != null && viewModel.getDestValue().getValue() != null) ? View.VISIBLE : View.GONE);
            if (currencyEntity != null) {
                textSourceCurrency.setText(currencyEntity.getName());
                viewModel.calculate(editSource.getText().toString());
            } else textSourceCurrency.setText(R.string.text_not_selected);
        });

        viewModel.getDestCurrency().observe(this, currencyEntity -> {
            viewInputs.setVisibility((currencyEntity != null && viewModel.getSourceCurrency().getValue() != null) ? View.VISIBLE : View.GONE);
            if (currencyEntity != null) {
                textDestCurrency.setText(currencyEntity.getName());
                viewModel.calculate(editSource.getText().toString());
            } else textDestCurrency.setText(R.string.text_not_selected);
        });

        viewModel.getDestValue().observe(this, value -> {
            if (value != null) textResult.setText(value);
        });

        viewModel.getLastUpdate().observe(this, lastUpdate -> {
            if (!TextUtils.isEmpty(lastUpdate)) {
                textLastUpdate.setText(getString(R.string.text_last_update, lastUpdate));
                textLastUpdate.setVisibility(View.VISIBLE);
            } else textLastUpdate.setVisibility(View.GONE);
        });
    }
}
