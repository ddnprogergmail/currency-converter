package ru.ddnproger.converter.ui.viewModel.currency.list;

import android.arch.lifecycle.MutableLiveData;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.ddnproger.converter.ui.view.currency.list.SelectionType;
import ru.ddnproger.converter.ui.common.BaseViewModel;
import ru.ddnproger.domain.entity.CurrencyEntity;
import ru.ddnproger.domain.interactor.CurrencyInteractor;

public class CurrencyListViewModel extends BaseViewModel {

    private final CurrencyInteractor interactor;

    private final MutableLiveData<List<CurrencyEntity>> currencies = new MutableLiveData<>();
    private final MutableLiveData<Boolean> currencyLoadError = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();

    @Inject
    public CurrencyListViewModel(CurrencyInteractor interactor) {
        this.interactor = interactor;
        fetch();
    }

    public MutableLiveData<List<CurrencyEntity>> getCurrencies() {
        return currencies;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public MutableLiveData<Boolean> getError() {
        return currencyLoadError;
    }

    public void retry() {
        fetch();
    }

    public void setCurrency(CurrencyEntity currency, SelectionType type) {
        if (type == SelectionType.SOURCE) {
            interactor.setSourceCurrency(currency);
        } else if (type == SelectionType.DEST) {
            interactor.setDestCurrency(currency);
        }
    }

    public CurrencyEntity getCurrentSource() {
        return interactor.getSourceCurrency();
    }

    public CurrencyEntity getCurrentDest() {
        return interactor.getDestCurrency();
    }

    private void fetch() {
        loading.setValue(true);

        Disposable disposable = interactor.getCurrencyList()
                .subscribe(currencyEntities -> {
                    currencyLoadError.setValue(false);
                    currencies.setValue(currencyEntities);
                    loading.setValue(false);
                }, throwable -> {
                    throwable.printStackTrace();
                    currencyLoadError.setValue(true);
                    loading.setValue(false);
                });

        unsubscribeOnDestroy(disposable);
    }
}
