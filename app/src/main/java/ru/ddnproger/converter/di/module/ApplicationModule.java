package ru.ddnproger.converter.di.module;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.ddnproger.data.database.CurrencyDatabase;
import ru.ddnproger.data.local.CurrencyStorage;
import ru.ddnproger.data.mapper.CurrencyDbEntityToEntityMapper;
import ru.ddnproger.data.mapper.CurrencyToDbEntityMapper;
import ru.ddnproger.data.network.SynchronizeNetwork;
import ru.ddnproger.data.repository.SynchronizeRepositoryImpl;
import ru.ddnproger.data.repository.CurrencyRepositoryImpl;
import ru.ddnproger.domain.common.SchedulerProvider;
import ru.ddnproger.domain.repository.SynchronizeRepository;
import ru.ddnproger.domain.repository.CurrencyRepository;

@Module(includes = {ViewModelModule.class, ContextModule.class})
public class ApplicationModule {

    @Provides
    @Singleton
    CurrencyDatabase provideCurrencyDatabase(Context context) {
        CurrencyDatabase database = Room.databaseBuilder(context, CurrencyDatabase.class, "currency.db")
                .build();

        return database;
    }

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider() {
            @Override
            public Scheduler io() {
                return Schedulers.io();
            }

            @Override
            public Scheduler ui() {
                return AndroidSchedulers.mainThread();
            }

            @Override
            public Scheduler computation() {
                return Schedulers.computation();
            }
        };
    }

    @Provides
    @Singleton
    SharedPreferences providePrefs(Context context) {
        return context.getSharedPreferences("currencyPrefs", Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    SynchronizeRepository provideSynchronizeRepository(SynchronizeNetwork network, CurrencyStorage currencyStorage, CurrencyToDbEntityMapper mapper) {
        return new SynchronizeRepositoryImpl(network, currencyStorage, mapper);
    }

    @Provides
    @Singleton
    CurrencyRepository provideCurrencyRepository(CurrencyDbEntityToEntityMapper mapper, CurrencyStorage storage) {
        return new CurrencyRepositoryImpl(mapper, storage);
    }

    @Provides
    @Singleton
    CurrencyStorage provideCurrencyStorage(SharedPreferences preferences, CurrencyDatabase database) {
        return new CurrencyStorage(preferences, database);
    }
}
