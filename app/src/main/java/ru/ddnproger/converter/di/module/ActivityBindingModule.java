package ru.ddnproger.converter.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.ddnproger.converter.ui.view.ApplicationActivity;
import ru.ddnproger.converter.ui.view.ApplicationFragmentBindingModule;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = {ApplicationFragmentBindingModule.class})
    abstract ApplicationActivity bindMainActivity();
}
