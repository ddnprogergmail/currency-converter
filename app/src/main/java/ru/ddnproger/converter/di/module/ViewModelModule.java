package ru.ddnproger.converter.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import ru.ddnproger.converter.di.util.ViewModelKey;
import ru.ddnproger.converter.ui.common.ViewModelFactory;
import ru.ddnproger.converter.ui.viewModel.currency.list.CurrencyListViewModel;
import ru.ddnproger.converter.ui.viewModel.currency.main.CurrencyMainViewModel;

@Singleton
@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CurrencyMainViewModel.class)
    abstract ViewModel bindCurrencyMainViewModel(CurrencyMainViewModel currencyMainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CurrencyListViewModel.class)
    abstract ViewModel bindCurrencyListViewModel(CurrencyListViewModel currencyListViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
