package ru.ddnproger.converter;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import ru.ddnproger.converter.di.ApplicationComponent;
import ru.ddnproger.converter.di.DaggerApplicationComponent;

public class BaseApplication extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent component = DaggerApplicationComponent.builder().application(this).build();
        component.inject(this);

        return component;
    }
}
