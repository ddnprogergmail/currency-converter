package ru.ddnproger.domain.entity;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CurrencyEntityTest {

    @Test
    public void calculateTest() {
        CurrencyEntity source = new CurrencyEntity("123", "Доллар", 1, BigDecimal.valueOf(66.0763));
        CurrencyEntity dest = new CurrencyEntity("321", "Евро", 1, BigDecimal.valueOf(74.2896));

        BigDecimal value = BigDecimal.valueOf(1);
        Assert.assertEquals(source.calculate(value, dest).toString(), source.getValue().divide(dest.getValue(), RoundingMode.CEILING).toString());
        Assert.assertEquals(dest.calculate(value, source).toString(), dest.getValue().divide(source.getValue(), RoundingMode.CEILING).toString());
    }
}
