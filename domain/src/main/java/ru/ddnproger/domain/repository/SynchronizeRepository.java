package ru.ddnproger.domain.repository;

import io.reactivex.Completable;

public interface SynchronizeRepository {

    Completable synchronize();
}
