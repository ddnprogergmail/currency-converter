package ru.ddnproger.domain.repository;

import java.util.List;

import io.reactivex.Single;
import ru.ddnproger.domain.entity.CurrencyEntity;

public interface CurrencyRepository {

    Single<List<CurrencyEntity>> getCurrencyList();

    String getLastUpdate();

    void setSourceCurrency(CurrencyEntity currency);

    void setDestCurrency(CurrencyEntity currency);

    CurrencyEntity getSourceCurrency();

    CurrencyEntity getDestCurrency();
}
