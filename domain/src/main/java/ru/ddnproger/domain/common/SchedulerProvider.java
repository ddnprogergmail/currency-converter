package ru.ddnproger.domain.common;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public interface SchedulerProvider {

    Scheduler io();
    Scheduler ui();
    Scheduler computation();
}
