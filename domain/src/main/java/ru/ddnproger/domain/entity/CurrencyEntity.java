package ru.ddnproger.domain.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.Currency;
import java.util.Locale;
import java.util.SortedMap;
import java.util.TreeMap;

public class CurrencyEntity {

    private String charCode;
    private String name;
    private int nominal;
    private BigDecimal value;

    private final SortedMap<Currency, Locale> currencyLocaleMap;

    public CurrencyEntity(String charCode, String name, int nominal, BigDecimal value) {
        this.charCode = charCode;
        this.name = name;
        this.nominal = nominal;
        this.value = value;
        currencyLocaleMap = initCurrencyLocaleMap();
    }

    public String getCharCode() {
        return charCode;
    }

    public String getName() {
        return name;
    }

    public int getNominal() {
        return nominal;
    }

    public BigDecimal getValue() {
        return value;
    }

    public BigDecimal getRubleForSingle() {
        BigDecimal result = value.divide(BigDecimal.valueOf(nominal), RoundingMode.CEILING);
        return result;
    }

    public BigDecimal calculateInRuble(BigDecimal value) {
        BigDecimal result = value.multiply(getRubleForSingle());
        return result;
    }

    public BigDecimal calculate(BigDecimal value, CurrencyEntity dest) {
        BigDecimal result = BigDecimal.ZERO;

        if (dest == null) {
            return result;
        }

        if (dest.getCharCode().equalsIgnoreCase(charCode)) {
            return value;
        }

        result = calculateInRuble(value).divide(dest.getRubleForSingle(), RoundingMode.CEILING);

        return result;
    }

    public String getCurrencySymbol() {
        try {
            Currency currency = Currency.getInstance(charCode);
            String symbol = currency.getSymbol(currencyLocaleMap.get(currency));
            return symbol;
        } catch (Exception e) {
            return "";
        }
    }

    private TreeMap<Currency, Locale> initCurrencyLocaleMap() {
        TreeMap<Currency, Locale> currencyLocaleMap = new TreeMap<>((c1, c2) -> c1.getCurrencyCode().compareTo(c2.getCurrencyCode()));
        for (Locale locale : Locale.getAvailableLocales()) {
            try {
                Currency currency = Currency.getInstance(locale);
                currencyLocaleMap.put(currency, locale);
            } catch (Exception e) {
            }
        }

        return currencyLocaleMap;
    }

    public boolean equals(CurrencyEntity currency) {
        if (currency == null) return false;

        return charCode.equalsIgnoreCase(currency.getCharCode());
    }
}
