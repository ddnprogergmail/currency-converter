package ru.ddnproger.domain.interactor;

import javax.inject.Inject;

import io.reactivex.Completable;
import ru.ddnproger.domain.common.SchedulerProvider;
import ru.ddnproger.domain.repository.SynchronizeRepository;

public class SynchronizeInteractor {

    private final SchedulerProvider schedulerProvider;
    private final SynchronizeRepository repository;

    @Inject
    public SynchronizeInteractor(SchedulerProvider schedulerProvider, SynchronizeRepository repository) {
        this.schedulerProvider = schedulerProvider;
        this.repository = repository;
    }

    public Completable synchronize() {
        return repository.synchronize()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui());
    }
}
