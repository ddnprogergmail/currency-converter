package ru.ddnproger.domain.interactor;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.ddnproger.domain.common.SchedulerProvider;
import ru.ddnproger.domain.entity.CurrencyEntity;
import ru.ddnproger.domain.repository.CurrencyRepository;

public class CurrencyInteractor {

    private final SchedulerProvider schedulerProvider;
    private final CurrencyRepository repository;

    @Inject
    public CurrencyInteractor(SchedulerProvider schedulerProvider, CurrencyRepository repository) {
        this.schedulerProvider = schedulerProvider;
        this.repository = repository;
    }

    public Single<List<CurrencyEntity>> getCurrencyList() {
        return repository.getCurrencyList()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui());
    }

    public void setSourceCurrency(CurrencyEntity currency) {
        repository.setSourceCurrency(currency);
    }

    public void setDestCurrency(CurrencyEntity currency) {
        repository.setDestCurrency(currency);
    }

    public CurrencyEntity getSourceCurrency() {
        return repository.getSourceCurrency();
    }

    public CurrencyEntity getDestCurrency() {
        return repository.getDestCurrency();
    }

    public String getLastUpdate() {
        return repository.getLastUpdate();
    }

    public void swapCurrency() {
        CurrencyEntity source = getSourceCurrency();
        CurrencyEntity dest = getDestCurrency();

        repository.setSourceCurrency(dest);
        repository.setDestCurrency(source);
    }
}
