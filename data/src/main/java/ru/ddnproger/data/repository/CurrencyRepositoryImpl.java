package ru.ddnproger.data.repository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import ru.ddnproger.data.local.CurrencyStorage;
import ru.ddnproger.data.mapper.CurrencyDbEntityToEntityMapper;
import ru.ddnproger.domain.entity.CurrencyEntity;
import ru.ddnproger.domain.repository.CurrencyRepository;

public class CurrencyRepositoryImpl implements CurrencyRepository {

    private final CurrencyDbEntityToEntityMapper mapper;
    private final CurrencyStorage storage;

    private CurrencyEntity source;
    private CurrencyEntity dest;

    @Inject
    public CurrencyRepositoryImpl(CurrencyDbEntityToEntityMapper mapper, CurrencyStorage storage) {
        this.mapper = mapper;
        this.storage = storage;
    }

    @Override
    public Single<List<CurrencyEntity>> getCurrencyList() {
        return storage.getCurrencyList()
                .flatMapObservable(Observable::fromIterable)
                .map(mapper::map)
                .toList();
    }

    @Override
    public String getLastUpdate() {
        return storage.getLastUpdate();
    }

    @Override
    public void setSourceCurrency(CurrencyEntity currency) {
        source = currency;
    }

    @Override
    public void setDestCurrency(CurrencyEntity currency) {
        dest = currency;
    }

    @Override
    public CurrencyEntity getSourceCurrency() {
        return source;
    }

    @Override
    public CurrencyEntity getDestCurrency() {
        return dest;
    }
}
