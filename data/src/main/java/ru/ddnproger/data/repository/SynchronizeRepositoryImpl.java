package ru.ddnproger.data.repository;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Observable;
import ru.ddnproger.data.local.CurrencyStorage;
import ru.ddnproger.data.mapper.CurrencyToDbEntityMapper;
import ru.ddnproger.data.network.SynchronizeNetwork;
import ru.ddnproger.domain.repository.SynchronizeRepository;

public class SynchronizeRepositoryImpl implements SynchronizeRepository {

    private final SynchronizeNetwork network;
    private final CurrencyStorage currencyStorage;
    private final CurrencyToDbEntityMapper currencyToDbEntityMapper;

    public SynchronizeRepositoryImpl(SynchronizeNetwork network, CurrencyStorage currencyStorage, CurrencyToDbEntityMapper currencyToDbEntityMapper) {
        this.network = network;
        this.currencyStorage = currencyStorage;
        this.currencyToDbEntityMapper = currencyToDbEntityMapper;
    }

    @Override
    public Completable synchronize() {
        return network.getCurrencyList()
                .flatMapObservable(currencyList -> {
                    currencyStorage.saveLastUpdate(currencyList.getDate());
                    currencyList.addRurCurrency();
                    return Observable.fromIterable(currencyList.getCurrencyList());
                })
                .map(currencyToDbEntityMapper::map)
                .toList()
                .flatMapCompletable(currencyList -> {
                    currencyStorage.saveCurrency(currencyList);
                    return CompletableObserver::onComplete;
                });
    }
}
