package ru.ddnproger.data.network;

import org.simpleframework.xml.core.Persister;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.ddnproger.data.models.CurrencyList;

public class SynchronizeNetwork {

    @Inject
    public SynchronizeNetwork() {
    }

    public Single<CurrencyList> getCurrencyList() {
        return Single.create(s -> {
            String xml = "";
            URL url;
            HttpURLConnection connection = null;

            try {
                url = new URL("http://www.cbr.ru/scripts/XML_daily.asp");
                connection = ((HttpURLConnection) url.openConnection());
                InputStream in = connection.getInputStream();

                InputStreamReader isw = new InputStreamReader(in, "CP1251");

                int data = isw.read();
                while (data != -1) {
                    xml += String.valueOf((char) data);
                    data = isw.read();
                }

                Reader reader = new StringReader(xml);
                Persister serializer = new Persister();
                CurrencyList currencyList = serializer.read(CurrencyList.class, reader, false);
                s.onSuccess(currencyList);
            } catch (MalformedURLException e) {
                s.onError(e);
            } catch (IOException e) {
                s.onError(e);
            }
        });


    }
}
