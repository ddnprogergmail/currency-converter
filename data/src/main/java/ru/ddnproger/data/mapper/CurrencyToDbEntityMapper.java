package ru.ddnproger.data.mapper;

import javax.inject.Inject;

import ru.ddnproger.data.database.entity.CurrencyDbEntity;
import ru.ddnproger.data.models.Currency;

public class CurrencyToDbEntityMapper {

    @Inject
    public CurrencyToDbEntityMapper() {
    }

    public CurrencyDbEntity map(Currency currency) {
        return new CurrencyDbEntity(
                currency.getId(),
                currency.getNumCode(),
                currency.getCharCode(),
                currency.getNominal(),
                currency.getName(),
                currency.getValue()
        );
    }
}
