package ru.ddnproger.data.mapper;

import java.math.BigDecimal;

import javax.inject.Inject;

import ru.ddnproger.data.database.entity.CurrencyDbEntity;
import ru.ddnproger.domain.entity.CurrencyEntity;

public class CurrencyDbEntityToEntityMapper {

    @Inject
    public CurrencyDbEntityToEntityMapper() {
    }

    public CurrencyEntity map (CurrencyDbEntity dbEntity) {
        return new CurrencyEntity(
                dbEntity.getCharCode(),
                dbEntity.getName(),
                dbEntity.getNominal(),
                new BigDecimal(dbEntity.getValue())
        );
    }
}
