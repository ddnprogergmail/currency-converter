package ru.ddnproger.data.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Single;
import ru.ddnproger.data.database.entity.CurrencyDbEntity;

@Dao
public interface CurrencyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<CurrencyDbEntity> currencyList);

    @Query("SELECT * FROM CurrencyDbEntity")
    Single<List<CurrencyDbEntity>> getCurrencyList();
}
