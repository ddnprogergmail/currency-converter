package ru.ddnproger.data.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import ru.ddnproger.data.database.dao.CurrencyDao;
import ru.ddnproger.data.database.entity.CurrencyDbEntity;

@Database(entities = {CurrencyDbEntity.class}, version = 51)
public abstract class CurrencyDatabase extends RoomDatabase {

    public abstract CurrencyDao currencyDao();
}
