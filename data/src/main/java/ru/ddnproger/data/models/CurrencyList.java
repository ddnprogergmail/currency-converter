package ru.ddnproger.data.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "ValCurs")
public class CurrencyList {

    @Attribute(name = "Date")
    String date;
    @Attribute(name = "name")
    String name;

    @ElementList(inline = true, name = "Valute")
    List<Currency> currencyList;

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public List<Currency> getCurrencyList() {
        return currencyList;
    }

    public void addRurCurrency() {
        getCurrencyList().add(generateRurCurrency());
    }

    private Currency generateRurCurrency() {
        Currency currency = new Currency();
        currency.setId("R00000");
        currency.setNumCode(643);
        currency.setCharCode("RU");
        currency.setNominal(1);
        currency.setName("Российский рубль");
        currency.setValue("1,00");
        return currency;
    }
}
