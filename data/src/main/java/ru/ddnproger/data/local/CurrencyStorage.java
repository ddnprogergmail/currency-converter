package ru.ddnproger.data.local;

import android.content.SharedPreferences;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.ddnproger.data.database.CurrencyDatabase;
import ru.ddnproger.data.database.entity.CurrencyDbEntity;

public class CurrencyStorage {

    private static final String LAST_UDPATE = "lastUpdate";

    private final SharedPreferences prefs;
    private final CurrencyDatabase database;

    @Inject
    public CurrencyStorage(SharedPreferences prefs, CurrencyDatabase database) {
        this.prefs = prefs;
        this.database = database;
    }

    public void saveCurrency(List<CurrencyDbEntity> currencyList) {
        database.currencyDao().insert(currencyList);
    }

    public void saveLastUpdate(String date) {
        prefs.edit().putString(LAST_UDPATE, date).apply();
    }

    public Single<List<CurrencyDbEntity>> getCurrencyList() {
        return database.currencyDao().getCurrencyList();
    }

    public String getLastUpdate() {
        String lastUpdate = prefs.getString(LAST_UDPATE, "");
        return lastUpdate;
    }
}
